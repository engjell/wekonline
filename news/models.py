from django.db import models
from django.forms.models import model_to_dict
from django.core.urlresolvers import reverse

# Create your models here.
class SignUp(models.Model):
  email = models.EmailField(max_length=254)
  full_name = models.CharField(max_length = 120, blank = True)
  created_time = models.DateTimeField(auto_now_add = True)
  last_modified = models.DateTimeField(auto_now = True)

  def __unicode__(self):
    dict = model_to_dict(self)
    return "\'{full_name}\' <{email}>".format(**dict)